# FSI Credit Card Dispute for Openshift s2i build
This is an aggregation of the individual modules of the FSI CCD app to enable Immutable Kie-Server s2i build on Openshift.

## About FSI CCD demo app
https://github.com/entando/fsi-credit-dispute-kie
## Original source codes
### DisputesCaseManagement
https://github.com/entando/fsi-credit-card-dispute-case
### DisputeCaseMgmtDecisions
https://github.com/entando/fsi-credit-card-dynamic-questions
### CreditDisputeModel
https://github.com/entando/fsi-credit-dispute-kie/tree/master/CreditDisputeModel
